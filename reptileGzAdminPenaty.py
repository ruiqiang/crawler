# -*- coding: utf-8 -*-

import requests
import re
import urllib


url_page_prefix = 'http://www.gz-l-tax.gov.cn/xxgk/xxgkmlV2/ztfl/xzzf/xzcf/'
pattern = """<a TARGET="_blank" href="(.*?)">"""
re_pa1 = re.compile(pattern)

context_pattern = """<title>(.*?)</title>[\s\S]*<meta name="Url" content="(.*?)">"""
context_re_pa = re.compile(context_pattern)


def gain_page_url(url, re_pa):
    html = requests.get(url=url,
                        headers={'Content-Type': 'text/html;charset=UTF-8',
                                 'User-Agent': 'Mozila/4.0(compatible;MSIE5.01;'
                                               'Window NT5.0)'})
    page_list = re_pa.findall(html.text)
    return page_list


def decode_encode(s):
    from array import array
    b = array('u', s)
    c = b.tostring()
    d = c[::2]
    return d.decode()


if __name__ == '__main__':
    url_info_list = []
    url_page_list = list(map(lambda x: url_page_prefix+x, ['index.html', 'index_1.html']))
    for i in url_page_list:
        url_info_list.extend(gain_page_url(i, re_pa1))

    for p in url_info_list:
        new_page = gain_page_url(p, context_re_pa)
        urllib.request.urlretrieve(new_page[0][1],
                                   u'F:\\贵州税务\\行政处罚\\' + decode_encode(new_page[0][0])
                                   +".xlsx")