# -*- coding: utf-8 -*-

import requests
import re
import urllib.request


url_page_prefix0 = 'http://www.sc-n-tax.gov.cn/xxfb/gsgg/'
url_page_prefix = 'http://www.sc-n-tax.gov.cn/xxfb/gsgg/index.html'

pattern = """<a title="(.*?)" target="_blank" href="(.*?)">"""
re_pa1 = re.compile(pattern)

context_pattern = """href=".(.*?)" target="_blank">(.*?)<"""
context_re_pa = re.compile(context_pattern)


def gain_page_url(url, re_pa):
    html = requests.get(url=url,
                        headers={'Content-Type': 'text/html;charset=UTF-8',
                                 'User-Agent': 'Mozila/4.0(compatible;MSIE5.01;'
                                               'Window NT5.0)'})
    page_list = re_pa.findall(html.text)
    return page_list


def decode_encode(s):
    from array import array
    b = array('u', s)
    c = b.tostring()
    d = c[::2]
    return d.decode()


if __name__ == '__main__':
    page_info_list = gain_page_url(url_page_prefix, re_pa1)
    res_list = []
    for i in page_info_list:
        if u'国家税务总局四川省税务局关于走逃、失踪户欠缴税款情况的公告' in decode_encode(i[0]):
            res_list.append(url_page_prefix0+i[1][2:])
    for j in res_list:
        new_page = gain_page_url(j, context_re_pa)
        new_url = re.sub(r'/t2.*$', new_page[0][0], j)
        urllib.request.urlretrieve(new_url,
                                   u'F:\\四川税务\\' + decode_encode(new_page[0][1]))
