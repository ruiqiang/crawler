# -*- coding: utf-8 -*-

import requests
import re
import numpy as np
import pandas as pd

url_page_prefix = 'http://www.gz-l-tax.gov.cn/xxgk/zdgk/zdsswfaj_33264/'
pattern = """href="(.*?)"  title="(.*?)">"""
re_pa = re.compile(pattern)


out_path = u'F:\\贵州重大税收违法案件\\贵州重大税收违法案件.xlsx'


def decode_encode(s):
    from array import array
    b = array('u', s)
    c = b.tostring()
    d = c[::2]
    return d.decode()


def gain_page_url(url):
    html = requests.get(url=url,
                        headers={'Content-Type': 'text/html;charset=UTF-8',
                                 'User-Agent': 'Mozila/4.0(compatible;MSIE5.01;'
                                               'Window NT5.0)'})
    page_list = re_pa.findall(html.text)
    return page_list


def gain_page_info(url):
    df_data_list = pd.read_html(url)
    return df_data_list[0]


def deal_df(df_data):
    df_to_dict = df_data.to_dict('list')
    res_dict = {}
    for c in range(len(df_to_dict[0])):
        if ':' in df_to_dict[0][c]:
            column1_list = df_to_dict[0][c].split(":")
            column2_list = df_to_dict[1][c].split(":")
            res_dict.update({column1_list[0]: column1_list[1],
                             column2_list[0]: column2_list[1]})
        elif '、' in df_to_dict[0][c]:
            key_list_temp = df_to_dict[0][c].split('、')
            key_name = re.compile(r"(.*)姓名").findall(key_list_temp[0])[0]
            key_card_list = key_list_temp[2].split(u"及")
            key_list = list(map(lambda x: key_name + x,
                                key_list_temp[:2] + key_card_list))
            if df_to_dict[1][c] is not np.nan:
                value_list_temp = df_to_dict[1][c].split('、')
                value_list_temp_len = len(value_list_temp)
                if value_list_temp_len <= 2:
                    value_list = value_list_temp[:value_list_temp_len] + \
                                 [np.nan]*(len(key_list) - value_list_temp_len)
                else:
                    card_value_list = value_list_temp[2].split(":")
                    value_list = value_list_temp[:2] + card_value_list
            else:
                value_list = [np.nan]*len(key_list)
            res_dict.update(dict(zip(key_list, value_list)))
        else:
            res_dict.update({df_to_dict[0][c]: df_to_dict[1][c]})
    return res_dict


if __name__ == '__main__':
    data_list = []
    page_list1 = []
    url_list = [url_page_prefix + "index.html"]
    url_list.extend([url_page_prefix + 'index_' + str(page) + '.html'
                     for page in range(1, 7)])
    for p in url_list:
        page_list1.extend(gain_page_url(p))
        
    for i in page_list1:
        df = gain_page_info(i[0])
        data_dict = deal_df(df)
        data_list.append(data_dict)
    df_data1 = pd.DataFrame(data_list)
    df_data1.to_excel(out_path, index=0)






