# -*- coding: utf-8 -*-

import requests
import re
import urllib.request
import os
import pandas as pd
import numpy as np

url_page_prefix = 'http://www.sc-n-tax.gov.cn/xxfb/sgs/xzcfgs/'
pattern = """a title=".*" target="_blank" href=".(.*?)">"""
re_pa1 = re.compile(pattern)

context_pattern = """<a href=".(.*?)" target="_blank">(.*?)<"""
context_re_pa = re.compile(context_pattern)


path = u'F:\\四川税务\\行政处罚\\'


def gain_page_url(url, re_pa):
    html = requests.get(url=url,
                        headers={'Content-Type': 'text/html;charset=UTF-8',
                                 'User-Agent': 'Mozila/4.0(compatible;MSIE5.01;'
                                               'Window NT5.0)'})
    page_list = re_pa.findall(html.text)
    return page_list


def decode_encode(s):
    from array import array
    b = array('u', s)
    c = b.tostring()
    d = c[::2]
    return d.decode()


if __name__ == '__main__':
    url_info_list = []
    url_page_list = list(map(lambda x:url_page_prefix+x, ['index.html','index_1.html']))
    for i in url_page_list:
        url_info_list.extend(gain_page_url(i, re_pa1))
    for p in url_info_list:
        new_page = gain_page_url(url_page_prefix+p, context_re_pa)
        new_url = re.sub(r'/t2.*$', new_page[0][0], url_page_prefix+p)
        urllib.request.urlretrieve(new_url, path + new_url.split("/")[-1])

    # 合并数据

    file_list = filter(lambda p: p.__contains__(".xls"), os.listdir(path))
    columns_list = ['行政处罚决定书文号', '案件名称', '处罚类别', '处罚事由',
                    '处罚依据', '行政相对人名称', '统一社会信用代码', '组织机构代码',
                    '工商登记码', '税务登记号', '居民身份证号', '法定代表人姓名',
                    '处罚结果', '处罚生效期', '处罚截止期', '处罚机关', '公示期限',
                    '当前状态', '地方编码', '数据更新时间', '备注', '录入日期']
    res_list = []

    for i in file_list:
        file_path = path + i
        df = pd.read_excel(file_path)
        columns = df.columns.tolist()
        intersection = list(set(columns_list) & set(columns))
        complement = list(set(columns_list) - set(columns))
        df_temp0 = df.loc[:, intersection]
        df_temp = df_temp0[df_temp0['统一社会信用代码'].isnull() == False]
        for c in complement:
            df_temp[c] = np.nan
        res_list.extend([df_temp])
    df_res = pd.concat(res_list, sort=True)
    df_res.to_excel(u'F:\\四川税务\\汇总信息\\行政处罚.xlsx', index=0)